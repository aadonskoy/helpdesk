require 'rails_helper'

feature 'Admin' do

  let(:admin) { create :admin }
  let(:ticket) { create :ticket }

  scenario 'Login as admin', versioning: true do
    tickid = ticket.tickid
    login

    visit "/tickets/#{tickid}"
    fill_in 'Subject', with: 'Extra subject'
    click_button 'Update Ticket'
    expect(page.body).to include("by #{admin.email}")
  end

  def login
    visit '/staffs/sign_in'
    fill_in 'Email', with: admin.email
    fill_in 'Password', with: admin.password
    click_button 'Log in'
  end
end

