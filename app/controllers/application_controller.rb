class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include Pundit
  protect_from_forgery with: :exception

  before_action :authenticate_staff!

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def current_user
    current_staff
  end

  protected

  def after_sign_in_path_for(resource)
    unassigned_staff_tickets_path
  end

  def after_sign_out_path_for(resource_or_scope)
    root_path
  end

  private

  def user_not_authorized
    flash[:alert] = 'You are not authorized to perform this action'
    redirect_to(request.referrer || unassigned_staff_tickets_path)
  end
end
