class Status < ActiveRecord::Base
  has_many :tickets

  validates :name, presence: true

  scope :ids_by_names, ->(names) {
    select(:id).where(name: names).pluck(:id)
  }
end
