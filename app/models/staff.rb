class Staff < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :role_id, presence: true

  belongs_to :role
  has_many :tickets

  def to_label
    email
  end

  def admin?
    'admin' == Role.find(role_id).name
  end
end
