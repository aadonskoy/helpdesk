module HelpdeskUtils
  def generate_random_ticket_token
    "#{random_three_chars}-#{random_three_digits}-#{random_three_chars}-#{random_three_digits}-#{random_three_chars}"
  end

  def random_three_digits
    result = ''
    3.times do
      result << rand(9).to_s
    end
    result
  end

  def random_three_chars
    chars = ('A'..'Z').to_a
    result = ''
    3.times do
      result << chars[rand(chars.length)]
    end
    result
  end
end
