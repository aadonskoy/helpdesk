class StaffTicketsController < ApplicationController
  before_action :get_ticket, only: [:show, :update_ticket]

  def unassigned
    @tickets = Ticket.unassigned
  end

  def on_hold
    @tickets = Ticket.on_hold
  end

  def opened
    @tickets = Ticket.opened
  end

  def closed
    @tickets = Ticket.closed
  end

  def show
    @ticket.comments.build
    @current_staff_id = current_staff.id
  end

  def update_ticket
    @ticket.update(ticket_params)
    redirect_to staff_ticket_path(@ticket.tickid),
                notice: 'Successfull updated ticket'
  end

  private

  def get_ticket
    @ticket = Ticket.find_by_tickid(params[:id])
  end

  def ticket_params
    params.require(:ticket).permit(:department_id,
                                   :staff_id,
                                   :status_id,
                                   comments_attributes: [:body, :staff_id])
  end
end
