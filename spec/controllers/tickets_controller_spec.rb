require 'rails_helper'

RSpec.describe TicketsController, type: :controller do
  let(:department) { create :department }
  let(:status) { create :status }
  let(:ticket) { create :ticket }

  describe "GET new" do
    it "returns http success" do
      department
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST create" do
    it "add ticket successfull" do
      expect do
        post :create, ticket: attributes_for(:ticket,
                                             department_id: department,
                                             status_id: status)
      end.to change{ Ticket.count }.by(1)
    end

    it 'render new on fail' do
      post :create, ticket: attributes_for(:ticket, name: '', department_id: department)
      expect(response).to render_template(:new)
    end
  end

  describe "GET show" do
    it "returns http success" do
      get :show, id: ticket.tickid
      expect(response).to have_http_status(:success)
    end
  end

end
