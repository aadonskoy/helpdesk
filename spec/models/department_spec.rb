require 'rails_helper'

RSpec.describe Department, :type => :model do
  it 'create departmants' do
    department_attr = attributes_for :department
    expect { Department.create(department_attr) }.to change { Department.count }.by(1)
  end
end
