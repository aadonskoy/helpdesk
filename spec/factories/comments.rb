FactoryGirl.define do
  factory :comment do
    staff
    body  { Faker::Lorem.words(5).join(' ') }
    ticket
  end

end
