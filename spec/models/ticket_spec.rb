require 'rails_helper'

RSpec.describe Ticket, :type => :model do

  let(:department) { create :department }
  let(:status) { create :status }

  it 'create ticket' do
    ticket_attr = attributes_for :ticket,
                                 department_id: department.id,
                                 status_id: status.id
    expect{ Ticket.create!(ticket_attr) }.to change{ Ticket.count }.by(1)
  end

  it 'add version on ticket update', versioning: true do
    ticket = create :ticket
    body = ticket.body
    ticket.update_attributes!(body: 'abracadabra')
    expect(ticket.reload.all_versions.last[:body]).to eq(body)
  end

  #TODO: add spec for unassigned, assigned, opened, closed, on_hold
end
