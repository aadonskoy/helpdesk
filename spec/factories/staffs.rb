FactoryGirl.define do
  factory :staff do
    email { Faker::Internet.email }
    password '12345678'
    password_confirmation '12345678'
  end

  factory :admin, parent: :staff do
    role { create(:role, name: 'admin') }
  end

  factory :member, parent: :staff do
    role { create(:role, name: 'member') }
  end
end
