class StaffMembersController < ApplicationController
  before_action :get_staff, only: [:update, :destroy, :edit_password, :update_password]
  def index
    @staff_new = Staff.new
    @staffs = Staff.all
    authorize @staffs
  end

  def create
    @staff = Staff.create(staff_params)
    authorize @staff
    if @staff.save
      redirect_to staff_members_path
    else
      redirect_to staff_members_path, alert: 'Error creating staff member'
    end
  end

  def update
    authorize @staff
    if @staff.update(staff_params)
      redirect_to staff_members_path
    else
      redirect_to staff_members_path, alert: 'Error editing staff member'
    end
  end

  def edit_password
    authorize @staff
  end

  def update_password
    authorize @staff
    if @staff.update(staff_params)
      sign_in @staff, bypass: true
      redirect_to root_path, notice: 'Password has been changed successfully'
    else
      render 'edit_password'
    end
  end

  def destroy
    authorize @staff
    staff = @staff.destroy
    redirect_to staff_members_path, notice: "#{staff.email} deteted successfully."
  end

  private
  def staff_params
    params.require(:staff).permit(:email, :password, :password_confirmation, :role_id)
  end

  def get_staff
    @staff = Staff.find_by_id(params[:id])
  end
end
