# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

%w(Sales TechOps).each do |department|
  Department.create(name: department)
end if Department.count == 0

%w(admin member).each do |role|
  Role.create(name: role)
end if Role.count == 0

['Waiting for Staff Response',
 'Waiting for Customer',
 'On Hold',
 'Cancelled',
 'Completed'].each do |status|
   Status.create(name: status)
 end if Status.count == 0

admin_role = Role.find_by_name('admin')
Staff.create!(email: 'admin@admin.com', password: '12345678', role: admin_role)
