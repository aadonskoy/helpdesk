require 'rails_helper'

RSpec.describe StaffMembersController, :type => :controller do
  let (:admin_role) { create :role, name: 'admin' }
  let (:member_role) {create :role, name: 'member' }
  let (:admin) { create :admin, role_id: admin_role.id }
  let (:new_admin) { create :admin, role_id: admin_role.id }
  let (:member) {create :member, role_id: member_role.id }

  before do
    sign_in admin
  end

  describe 'get index' do
    it 'returns http success' do
      get :index
      expect(response).to have_http_status(:success)
    end

    it 'fail for member' do
      sign_in member
      get :index
      expect(flash[:alert]).to eq('You are not authorized to perform this action')
    end
  end

  describe 'get edit_password' do
    it 'returns http success' do
      get :edit_password, id: admin.id
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST create' do
    it 'successfully create staff' do
      expect do
        post :create, staff: attributes_for(:admin, role_id: 1)
      end.to change{ Staff.count }.by(1)
    end

    it 'fail if data wrong' do
      post :create, staff: {email: 'aaaaa'}
      expect(flash[:alert]).to include('Error creating staff member')
    end
  end

  describe 'PATCH update' do
    it 'successfully change role_id' do
      id = new_admin.id
      expect do
        patch :update, staff: { role_id: 2 }, id: id
      end.to change{ Staff.find(id).role_id }.to(2)
    end

    it 'fail with empty role_id' do
      id = new_admin.id
      patch :update, staff: { role_id: '' }, id: id
      expect(flash[:alert]).to include('Error editing staff member')
    end
  end

  describe 'PATCH update_password' do
    it 'successfully update password' do
      password = '1z2a3q3e'
      patch :update_password,
            id: admin.id,
            staff: { password: password,
                     password_confirmation: password }
      expect(flash[:notice]).to eq('Password has been changed successfully')
    end

    it 'fail when password and password confirmation did not match' do
      admin = Staff.first
      password = '1z2a3q3e'
      patch :update_password,
            id: admin.id,
            staff: { password: password,
                     password_confirmation: '1' }
      expect(response).to render_template(:edit_password)
    end
  end

  describe 'DELETE member' do
    it 'delete staff' do
      id = new_admin.id
      expect do
        delete :destroy, id: id
      end.to change(Staff, :count).by(-1)
    end
  end

end
