class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string     :name, null: false
      t.string     :subject, null: false
      t.string     :email, null: false
      t.string     :tickid, null: false, index: true
      t.text       :body, null: false
      t.references :department, index: true

      t.timestamps
    end
  end
end
