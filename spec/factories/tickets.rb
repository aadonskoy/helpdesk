FactoryGirl.define do
  factory :ticket do
    name        { Faker::Name.name }
    subject     { Faker::Lorem.sentence }
    email       { Faker::Internet.email }
    body        { Faker::Lorem.sentences(3).join(' ') }
    department
    status
  end

end
