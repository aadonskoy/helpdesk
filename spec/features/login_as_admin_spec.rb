require 'rails_helper'

feature 'Admin' do
  let(:admin) { create :admin }
  scenario 'Login as admin' do
    login
    expect(current_path).to eq '/staff_tickets/unassigned'
  end

  scenario 'Logout admin' do
    login
    click_on 'Log Out'
    expect(current_path).to eq '/'
  end

  def login
    visit '/staffs/sign_in'
    fill_in 'Email', with: admin.email
    fill_in 'Password', with: admin.password
    click_button 'Log in'
  end
end
