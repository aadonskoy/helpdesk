class TicketObserver < ActiveRecord::Observer
  def after_create(ticket)
    p ticket
    TicketMailer.notify_customer(ticket).deliver
  end
end
