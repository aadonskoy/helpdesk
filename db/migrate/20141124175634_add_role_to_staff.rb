class AddRoleToStaff < ActiveRecord::Migration
  def change
    add_reference :staffs, :role, index: true
  end
end
