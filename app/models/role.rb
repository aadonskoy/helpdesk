class Role < ActiveRecord::Base
  has_many :staffs

  validates :name, presence: true, uniqueness: true
end
