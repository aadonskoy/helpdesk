class StaffPolicy < ApplicationPolicy
    def index?
      user.admin?
    end

    def update?
      user.admin?
    end

    def create?
      user.admin?
    end

    def destroy?
      user.admin?
    end

    def edit_password?
      record.id == user.id
    end

    def update_password?
      edit_password?
    end
end
