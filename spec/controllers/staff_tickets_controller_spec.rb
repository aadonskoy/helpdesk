require 'rails_helper'

RSpec.describe StaffTicketsController, :type => :controller do
  login_admin

  let(:ticket) { create :ticket }
  let(:status) {create :status }

  it 'GET unassigned tickets' do
    get :unassigned
    expect(response.status).to eq(200)
  end

  it 'GET on hold tickets' do
    get :on_hold
    expect(response.status).to eq(200)
  end

  it 'GET opened' do
    get :opened
    expect(response.status).to eq(200)
  end

  it 'GET unassigned tickets' do
    get :closed
    expect(response.status).to eq(200)
  end

  it 'GET show ticket' do
    get :show, id: ticket.tickid
    expect(response.status).to eq(200)
  end

  it 'PATCH ticket' do
    patch :update_ticket,
          id: ticket.tickid,
          ticket: { status_id: status.id }
    expect(ticket.reload.status.id).to eq(status.id)
  end
end
