require 'rails_helper'

feature 'Customer' do

  let(:department) { create :department }
  let(:status) { create :status }

  scenario 'Create new ticket' do
    department
    status
    ticket_attr = attributes_for(:ticket)
    visit '/'
    fill_in 'Name', with: ticket_attr[:name]
    fill_in 'Email', with: ticket_attr[:email]
    fill_in 'Subject', with: ticket_attr[:subject]
    fill_in 'Body', with: ticket_attr[:body]
    click_button 'Create Ticket'
    ticket = Ticket.last
    expect(current_path).to eq "/tickets/#{ticket.tickid}"
  end

  scenario 'Edit existing ticket' do
    ticket = create :ticket
    new_subject = 'The Newest subject'

    visit "/tickets/#{ticket.tickid}"
    fill_in 'Subject', with: new_subject
    click_button 'Update Ticket'
    expect(ticket.reload.subject).to eq(new_subject)
  end

  scenario 'Fail update existing ticket wrong data' do
    ticket = create :ticket

    visit "/tickets/#{ticket.tickid}"
    fill_in 'Subject', with: ''
    click_button 'Update Ticket'
    expect(page.body).to include("can't be blank")
  end
end

