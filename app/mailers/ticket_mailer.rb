class TicketMailer < ActionMailer::Base
  default from: "info@helpdesk.com"

  def notify_customer(ticket)
    @mail = ticket
    mail(to: ticket.email,
         subject: ticket.subject,
         template_path: 'ticket_mailer',
         template_name: 'notify_customer'
        )
  end
end
