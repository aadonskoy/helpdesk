FactoryGirl.define do
  factory :status do
    name { Faker::Lorem.words(2).join(' ') }
  end

end
