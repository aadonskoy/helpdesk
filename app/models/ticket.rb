class Ticket < ActiveRecord::Base
  include HelpdeskUtils
  attr_accessor :capcha

  has_paper_trail on: [:update]

  has_many :comments, dependent: :destroy
  accepts_nested_attributes_for :comments,
                                reject_if: ->(obj) { obj[:body].blank? }

  belongs_to :department
  belongs_to :status
  belongs_to :staff

  before_create :generate_tickid
  before_create :set_status

  validates :name, :subject, :body, :department, presence: true
  validates :email, email_format: {
    message: "doesn't look like an email address"
  }
  validate  :check_for_spam_bot

  scope :unassigned, -> { where(staff: nil) }
  scope :assigned, -> { where.not(staff: nil) }
  scope :on_hold, -> {
    where(status_id: Status.ids_by_names('On Hold')).assigned
  }
  scope :opened, -> {
    where.not(status_id: Status.ids_by_names(['On Hold', 'Cancelled', 'Completed'])).assigned
  }
  scope :closed, -> {
    where(status_id: Status.ids_by_names(['Cancelled', 'Completed'])).assigned
  }

  def all_versions
    versions.map do |version|
      {
        event: version.event,
        author: Staff.find_by_id(version.whodunnit).try(:email) || 'customer',
        name: version.reify.name,
        status: version.reify.status.name,
        email: version.reify.email,
        department: version.reify.department.name,
        subject: version.reify.subject,
        body: version.reify.body,
        date: version.created_at
      } unless version.object.nil?
    end.compact
  end

  private

  def generate_tickid
    self.tickid = loop do
      gen_tickid = generate_random_ticket_token
      break gen_tickid unless Ticket.exists?(tickid: gen_tickid)
    end
  end

  def set_status
    self.status_id = self.status_id || 1
  end

  def check_for_spam_bot
    errors.add(:capcha, 'Spam protection') if capcha.present?
  end
end

