require "rails_helper"

RSpec.describe TicketMailer, :type => :mailer do
  let(:ticket) { create :ticket }
  let(:mail)   { TicketMailer.notify_customer(ticket) }

  it 'success send mail with certain subject' do
    expect(mail.subject).to eq(ticket.subject)
  end
end
