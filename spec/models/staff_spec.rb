require 'rails_helper'

RSpec.describe Staff, :type => :model do
  let(:staff) { create :admin }
  it 'returns email on to_lbel call' do
    expect(staff.email).to eq(staff.to_label)
  end
end

