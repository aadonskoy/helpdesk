class TicketsController < ApplicationController
  skip_action_callback :authenticate_staff!

  before_action :get_ticket, only: [:show, :update]

  def new
    @ticket = Ticket.new
    @departments = Department.all.map { |dep| [dep.name, dep.id] }
  end

  def create
    @ticket = Ticket.new(ticket_params)
    if @ticket.save
      redirect_to ticket_path(@ticket.tickid), notice: 'Ticket was created successfully.'
    else
      render :new
    end
  end

  def show
  end

  def update
    p @ticket
    if @ticket.update(ticket_params)
      redirect_to ticket_path(@ticket.tickid), notice: 'Ticket updated successfully'
    else
      render :show
    end
  end

  private
  def ticket_params
    params.require(:ticket).permit(:name,
                                   :email,
                                   :subject,
                                   :body,
                                   :department_id,
                                   :capcha,
                                   :status_id)
  end

  def get_ticket
    @ticket = Ticket.find_by_tickid(params[:id])
  end
end

