# Helpdesk

**Setup project:**


```
#!ruby

  bundle install
  rake db:setup
```


**Run tests:**


```
#!ruby

  rspec
```

